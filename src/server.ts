import { app } from './app';
import dotenv from 'dotenv';
import "./database";

dotenv.config();

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
})
