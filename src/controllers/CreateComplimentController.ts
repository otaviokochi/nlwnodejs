import { Request, response, Response } from "express";
import { CreateComplimentService } from "../services/CreateComplimentService";

export class CreateComplimentController {
  async handle(req: Request, res: Response) {
    const { tag_id, message, user_receiver } = req.body;
    const user_sender = req.user_id;
    const createComplimentService = new CreateComplimentService();
    
    const compliment = await createComplimentService.execute({
      tag_id,
      user_sender,
      user_receiver,
      message
    })

    return response.json(compliment);
    
  }
}