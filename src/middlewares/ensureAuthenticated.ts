import { Request, Response, NextFunction } from "express"
import jwt from 'jsonwebtoken';

interface IPayload {
  sub: string;
}

export const ensureAuthenticated = (request: Request, response: Response, next: NextFunction) => {
  const [, token] = request.headers.authorization.split(' ');

  if (!token) return response.status(401).end()

  try {
    const { sub } = jwt.verify(token, process.env.AUTH_SECRET) as IPayload;

    request.user_id = sub;

    return next();
  } catch (error) {
    return response.status(401).end()
  }
}