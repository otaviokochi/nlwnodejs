import { getCustomRepository } from "typeorm";
import { TagsRepositories } from "../repositories/TagsRepositories";


export class CreateTagService {
  async execute(name: string) {
    if (!name) throw new Error("Nome é necessário!");
    const tagRepositories = getCustomRepository(TagsRepositories);
    const tagAlreadyExists = await tagRepositories.findOne({ name });
    if(tagAlreadyExists) throw new Error("Tag já existente!");
    const tag = tagRepositories.create({
      name
    })
    tagRepositories.save(tag)
    return tag;
  }
}