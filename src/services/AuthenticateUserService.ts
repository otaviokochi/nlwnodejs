import bcrypt from 'bcrypt';
import { getCustomRepository } from 'typeorm';
import { UsersRepositories } from '../repositories/UsersRepositories';
import jsonwebtoken from 'jsonwebtoken';

interface IAuthenticateRequest {
  email: string,
  password: string
}

export class AuthenticateUserService {
  async execute({ email, password }: IAuthenticateRequest) {
    if (!email) throw new Error("Credenciais erradas!");
    const usersRepository = getCustomRepository(UsersRepositories);
    const user = await usersRepository.findOne({ email });
    if (!user) throw new Error("Credenciais erradas!");
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) throw new Error("Credenciais erradas!");
    
    return jsonwebtoken.sign({
      email,
      name: user.name,

    }, process.env.AUTH_SECRET, {
      subject: user.id,
      expiresIn: "3d"
    })
  }
}