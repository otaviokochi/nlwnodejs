import { getCustomRepository } from "typeorm";
import { UsersRepositories } from "../repositories/UsersRepositories";
import bcrypt from 'bcrypt';

interface IUserRequest {
  name: string,
  email: string,
  admin?: boolean,
  password: string
}

export class CreateUserService {

  async execute({ name, email, admin = false, password }: IUserRequest) {
    if (!email) throw new Error("Email missing!");
    const usersRepository = getCustomRepository(UsersRepositories);
    const userAlreadyExists = await usersRepository.findOne({
      email
    })
    if (userAlreadyExists) throw new Error("User already exists!");

    const passwordHash = await bcrypt.hash(password, 10);

    const user = usersRepository.create({
      name,
      email,
      admin,
      password: passwordHash
    })

    await usersRepository.save(user);
    return user;
  }
}